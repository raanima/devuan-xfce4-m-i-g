#!/usr/bin/env bash

## configure and install minimal xfce desktop environment

## check for sudo/root
#if ! [ $(id -u) = 0 ]; then
#  echo "This script must run with sudo, try again..."
#  exit 1
#fi

cat ./xsessionrc >> /home/$SUDO_USER/.xsessionrc
chown $SUDO_USER:$SUDO_USER /home/$SUDO_USER/.xsessionrc

apt update

    apt install --no-install-recommends \
    xorg \
    mesa-vulkan-drivers \
    vulkan-icd \
    xserver-xorg-video-intel \
    xfonts-cyrillic \
    mesa-utils 

apt install -y \
    libxfce4ui-utils \
    ibrsvg2-common \
    plymouth-label \
    xfce4-power-manager \
    xfce4-appfinder \
    xfce4-panel \
    xfce4-session \
    xfce4-settings \
    xfce4-terminal \
    xfconf \
    xfdesktop4 \
    xfwm4 \
    adwaita-qt \
    qt5ct \
    ntfs-3g \
    screenfetch 

apt-get remove thunar
apt-get autoremove
apt update

apt install --no-install-recommends -y \
    thunar \
    thunar-archive-plugin \
    thunar-volman \
    gtk2-engines \
    mousepad \
    gnome-mpv \
    qmmp \
    viewnior \
    feh \
    plank \
    network-manager \
    slim \
    unzip \
    zip \
    arj \
    lzip \
    lzop \
    ncompress \
    rzip \
    squashfs-tools \
    unace \
    unalz \
    unar \
    lrzip \
    p7zip \
    p7zip-full \
    gufw 
 
echo 
echo xfce install complete, please reboot and issue 'startx'
echo

